module Main where

import Lib

main :: IO ()
main = isolateText (src "fry.jpg") (dest "fry-text.jpg")

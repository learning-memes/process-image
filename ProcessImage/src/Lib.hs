{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
module Lib
    ( isolateText
    , src
    , dest
    ) where

import Control.Monad
import Data.List
import Graphics.Image

isWhite r g b
  = Data.List.and . fmap (0.9 <=) $ [r,g,b] 

contrast :: Pixel RGB Double -> Pixel RGB Double
contrast (PixelRGB r g b)
  | isWhite r g b = PixelRGB 1 1 1 
  | otherwise = PixelRGB 0 0 0 

data ImgType = Src | Dest
data Img (t :: ImgType) = Img FilePath 

src :: FilePath -> Img Src
src = Img

dest :: FilePath -> Img Dest
dest = Img

isolateText :: Img Src -> Img Dest -> IO ()
isolateText (Img src) (Img dest)
  = writeImage dest
  . Graphics.Image.map contrast
  . resize Bilinear Edge (500, 500)
  =<< readImageRGB VU src
